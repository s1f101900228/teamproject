from django.shortcuts import render
import base64
import json
import requests
import sys
from googletrans import Translator
from datetime import datetime
import requests
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
translator = Translator()
jp_words = []
en_words = []
str_api_key = "AIzaSyC4dq5W4xgu91kav7K6JVCLBlDi2OcZhoY"
image = []##画像のURL
name = []##料理名
description = []##料理の説明
url = []##料理のURL
marerial =[]##料理の材料
key = []
r = []
pas = ""


def recipe(request):
    recipe_get()
    recipe = {
        'image' : image[0],
        'name' : name[0],
        'description' : description[0],
        'url' : url[0],
    }
    return render(request, 'recipe.html',recipe)

def result(request):
    pas = request.post("pass")
    getRecogImageLA(pas)
    a1 = jp_words[0]
    a2 = jp_words[1]
    a3 = jp_words[2]
    a4 = jp_words[3]
    component = {
        'r1' : a1,
        'r2' : a2,
        'r3' : a3,
        'r4' : a4,
    }
    return render(request, 'result.html' ,component)

def top(request):
    return render(request, 'top.html')






def trans():
    for src in en_words:
        dst = translator.translate(src, src='en', dest='ja')
        jp_words.append(dst.text)

def getRecogImageLA(image):
    image = open(image, 'rb').read()
    ENCODING = 'utf-8'
    str_encode_file = base64.b64encode(image)
    str_decode_file = str_encode_file.decode(ENCODING)

    str_url = "https://vision.googleapis.com/v1/images:annotate?key="
    str_headers = {'Content-Type': 'application/json'}
    url = str_url + str_api_key

    str_json_data = {
        'requests': [
            {
                'image': {
                    'content': str_decode_file
                },
                'features': [{
                    'type': "LABEL_DETECTION",
                    'maxResults': 10
                }]
            }
        ]
    }

    jd = json.dumps(str_json_data)
    print("begin request")

    s = requests.Session()
    r = requests.post(url, data=jd, headers=str_headers)

    print("status code : ", r.status_code)
    print("end request")

    if r.status_code == 200:
        parsed = json.loads(r.text)
        for labelann in parsed["responses"][0]["labelAnnotations"]:
            en_words.append(labelann["description"])
        trans()

    else:
        print("error statsus code : ", r.status_code)
        return "error"



def recipe_get():
    ##jsonファイルを入れる場所 
    cred = credentials.Certificate('/Users/kumagaisatoya/Downloads/search-for-recipe-iniad-firebase-adminsdk-z8vso-cd3e3155b7.json')
    ##cred = credentials.Certificate("/Users/クマ/Downloads/search-for-recipe-iniad-firebase-adminsdk-z8vso-6cb14a137f.json")
    firebase_admin.initialize_app(cred, {
        'databaseURL': 'https://search-for-recipe-iniad.firebaseio.com/',
        'databaseAuthVariableOverride': {
            'uid': 'my-service-worker'
        }
    })
    users_ref = db.reference('/recipe')
    json = users_ref.get()
    for jsn_key in json:
        key.append(jsn_key)
    for i in range(0,135):
        match = json[key[i]]["material"]
        if('卵' in match):
            r.append(i)
            marerial.append(match)

    c = 0
    for jsn_key in json:
        if(c in r):
            name.append(jsn_key)
        c = c + 1

    if not r:
        print("料理が見つかりませんでした。")
    else:
        for i in r:
            image.append(json[key[i]]["image"])
            description.append(json[key[i]]["description"])
            url.append(json[key[i]]["url"])